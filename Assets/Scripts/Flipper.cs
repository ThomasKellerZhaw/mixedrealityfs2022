using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public enum Side
{
    left,
    right
}

public class Flipper : MonoBehaviour
{
    [Tooltip("Left or right flipper")]
    [SerializeField]
    Side side = Side.left;

    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnFlipLeft()
    {
        if (side==Side.right) return;
        anim.SetTrigger("flip");
        Debug.Log("OnFlipLeft");
    }

    public void OnFlipRight()
    {
        if (side==Side.left) return;
        anim.SetTrigger("flip");
        Debug.Log("OnFlipRight");
    }

    public void OnMove(InputValue value)
    {
    }
}
