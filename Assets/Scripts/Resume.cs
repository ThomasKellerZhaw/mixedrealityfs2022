using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resume : MonoBehaviour
{
    public GameObject prefab;
    public List<Transform> spawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);

        Instantiate(prefab, spawnPoints[UnityEngine.Random.Range(0, 6)]);
    }
}
