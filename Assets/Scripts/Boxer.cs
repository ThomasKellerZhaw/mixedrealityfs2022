using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxer : MonoBehaviour
{
    public float force = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        Rigidbody ball = other.gameObject.GetComponent<Rigidbody>();
        Vector3 vBall = ball.velocity;
        ball.AddForce(vBall * force, ForceMode.Impulse);
    }
}
